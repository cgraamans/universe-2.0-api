module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		if($state.user.auth){

				app.DB.q("SELECT email FROM users WHERE id = ?",[$state.user.auth.id])
					.then(hasEmail=>{

						if(hasEmail.length>0){
		
							$state.socket.emit(socketName,{ok:true,email:hasEmail[0].email});
							resolve(data);
						
						}

					})
					.catch(e=>{

						$state.socket.emit(socketName,{ok:false,msg:'error fetching email',e:e});
						resolve(data);

					});

		} else {

			$state.socket.emit(socketName,{ok:false,msg:'not logged in'});
			resolve(data);

		}

	});

};