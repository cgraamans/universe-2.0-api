module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		let proceed = true;
		if(!data.password) {
			$state.socket.emit(socketName,{ok:false,msg:'no password'});
			proceed = false;
		} else {

			let pass = app.AUTH.verifyPassword(data.password);
			if(pass.errors.length > 0){
				$state.socket.emit(socketName,{ok:false,msg:'invalid password',result:pass.errors});
				proceed = false;
			}
		}
		if(!$state.user.auth){
			$state.socket.emit(socketName,{ok:false,msg:'not logged in'});	
			proceed = false;
		}
		if(proceed){

			app.DB.q('UPDATE `users` SET password = ? WHERE id = ?',[app.AUTH.hash(data.password),$state.user.auth.id])
				.then(()=>{

					$state.socket.emit(socketName,{ok:true});

				})
				.catch(e=>{
					
					$state.socket.emit(socketName,{ok:false,msg:'password update failed',e:e});

				});

		}
		resolve();

	});

};