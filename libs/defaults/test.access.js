module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		$state.socket.emit(socketName,{ok:true,data:data});
		app.LOG.put('D','test.access success');

		resolve(data);

	});

};