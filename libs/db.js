let dbObj = class dbObj {
	
	constructor(options){

		let mysql = require('mysql');
		this.pool = mysql.createPool(options.connection);

		if(!options.retries) options.retries = 10
		this.options = options.recursive;

	}

	// Helper promise to connect to pool with retries
	getPoolConnector(counter=0){

		let that = this;
		return new Promise((resolve,reject)=> {
			
			if(that.pool) {

				that.pool.getConnection((err, connection)=>{

					if(err) {

						if(counter < that.options.retries) {

							setTimeout(()=>{

								that.getConnection(counter)
									.then(r=>{

										resolve(r);

									})
									.catch(e=>{

										reject(e);
										
									});

							},that.options.interval);
							
						} else {

							reject(err);

						}
						counter++;
					
					} else {

						resolve(connection);

					}

				});

			} else {

				reject('No Pool.');

			}

		});

	}

	execute(query,values,connection) {

		return new Promise((resolve,reject)=>{

			connection.query(query,values,(error,res)=>{

				if(error) {
				
					reject(error);
				
				} else {

					resolve(res);

				}

			});

		});

	}

	// start transaction
	transact(query,values,connection){

		let that = this;
		return new Promise((resolve,reject)=>{
		
			that.execute('START TRANSACTION;',[],connection)
				.then(()=>{

					that.execute(query,values,connection)
						.then(res=>{

							that.commit(connection)
								.then(()=>{

									resolve(res);

								})
								.catch(e=>{

									that.rollback()
										.then(()=>{

											reject(e);

										})
										.catch(e=>{

											reject(e);

										});

								});

						})
						.catch(e=>{

							that.rollback()
								.then(()=>{

									reject(e);

								})
								.catch(e=>{

									reject(e);

								});

						});

				})
				.catch(e=>{

					reject(e);

				});

		});

	}

	// commit transaction
	commit(connection) {

		let that = this;
		return new Promise((resolve,reject)=>{

			that.execute('COMMIT;',[],connection)
				.then(()=>{	

					resolve();
				
				})
				.catch(e=>{

					reject(e);

				});

		});

	}

	// rollback transaction
	rollback(connection) {

		let that = this;
		return new Promise((resolve,reject)=>{

			that.execute('ROLLBACK;',[],connection)
				.then(()=>{	

					resolve();
				
				})
				.catch(e=>{

					reject(e);

				});

		});

	}

	q(query,values,isTransaction) {

		let that = this;
		return new Promise((resolve,reject)=>{

			that.getPoolConnector()
				.then(connection=>{

					if(!isTransaction){

						that.transact(query,values,connection)
							.then(res=>{

								connection.release();
								resolve(res);
							
							})
							.catch(e=>{
							
								connection.release();
								reject(e);
							
							});

					} else {

						that.execute(query,values,connection)
							.then(res=>{

								connection.release();
								resolve(res);
							
							})
							.catch(e=>{

								connection.release();			
								reject(e);
							
							});

					}

				})
				.catch(e=>{

					reject(e);
				
				});

		});
	
	}

}
module.exports = options=>{

	let db = new dbObj(options);

	// mirror query for ease of use
	db.query = db.q;
	
	return db;

};