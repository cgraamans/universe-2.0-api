// Routing settings

// Switches
// 
// auth 		- boolean	- only allow for authenticated users. (default:false)
// noRoute		- boolean	- allow the socket data to pass through to the app's state for subsequent action (as in an 'after' hook). (default:false)
// immutable	- boolean	- set the data to be immutable when passed to the app's state for subsequent action (default:false)
// level 		- int		- admin level required for controller (default 0);
//							  * if level is set, auth to true

'use strict'
module.exports = {

	// Custom controllers
	controllers:{

		"example":{
			controller:"src/controllers/example.js",
		},

		"auth.register":{
			controller:"src/controllers/user.system.create.js",
		},

		"get.clusters.active":{
			controller:"src/controllers/get.clusters.active.js"
		},
		
		"get.cluster":{
			controller:"src/controllers/get.cluster.js"
		},
		
		"get.stars":{
			controller:"src/controllers/get.stars.js"
		},
		
		"get.obj":{
			controller:"src/controllers/get.obj.js"
		},
		
	},

};