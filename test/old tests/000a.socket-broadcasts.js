var should = require('should');
var io = require('socket.io-client');
var jwt = require('jsonwebtoken');

var socketURL = 'http://0.0.0.0:9001';

var options ={
	transports: ['websocket'],
	'force new connection': true
};

let userSecret = false;
let clientA = false;
let clientB = false;
describe("API - socket broadcasts",()=>{

	beforeEach(()=>{

		clientA = io.connect(socketURL, options);
		clientB = io.connect(socketURL, options);

	});

	afterEach(()=>{

		clientA.disconnect();
		clientB.disconnect();

		userSecretA = false;
		userSecretB = false;

	});

	it('client B must receive client A broadcast', function(done) {

		clientA.on('connect',connectionData=>{

			clientA.on('init', data=>{
				
				userSecretA = data.secret;
				console.log('ClientA',userSecretA);

				clientA.on('hello',helloData=>{
					console.log('hello received on A',helloData);
				});

			});

		});

		clientB.on('connect',connectionData=>{

			clientB.on('init', data=>{
				
				userSecretB = data.secret;
				console.log('ClientB',userSecretB);

				clientB.on('hello',helloData=>{
					console.log('hello received on B',helloData);
				});

			});

		});

	});

});