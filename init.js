'use strict';
var app = require('./libs/app.js');
app.init()
	.then(()=>{

		app.LOG.put('D',`process ${process.pid} running.`);

		// // clear all game.users
		// app.DB.q('UPDATE `game.users` SET isOnline = 0',[],true);
		// app.LOG.put('D','Cleared all game users\' online statuses');
		
		app.io.on('connection',socket=>{

			app.LOG.put('D',`user connected`);
			let connection = require('./libs/state.js')(app,socket);
			connection.then($state=>{

				// $state.socket can still be used as regular service
				// see https://socket.io for how.
				
			})
			.catch(e=>{
				
				app.LOG.put('E',e,true);
			
			});
			
		});

 	})
 	.catch(e=>{

 		console.log(e);

 	});

