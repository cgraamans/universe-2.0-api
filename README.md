# universe-api, fork of sockey2

See [sockey2](https://bitbucket.org/cgraamans/sockey2) for install info

## Install

import `/docs/sql/data.sql`

### Test setup

    npx nodemon init.js

## Models

### global.geo

Functions  
 - toCartesian  
 - boundingBox

### global.maths

Functions  
  - d100  
  - roundTo  
  - unixTime  
  - randFromMinMax  
  - randJitter  
  - randArrIndex

### system.create.objects

Functions
 - create(system)

### system.create

Functions  
 - create(numberToCreate,clusterId?)  
   - clusterRandom  
   - systemName  
   - position  
   - positionWalk  
   - positionSystemRecurse  
   - mapLead  
   - insertSystem  
   - setStars  
   - setMapLeader

Logic    
- selects a random cluster or indicated cluster 
- selects a system from that cluster that is not an active mapLeader  
- sets mapLeader tag in database  
- creates named list by `numberToCreate`
- recursive walk function  
  - given a position based on random mapLeader, not nearby existing stars in the database (recursive check of bounding box)  
  - added to mapLeader list  
  - added to systems list  
  - removed from process list  
- insert systeminto database  
- unset mapLeader tag in database
- uses list to generate stars and insert them into database


    wants:
    glowing line between planets = trade
    space debris when moving
    menu opts
    \- back
    \- events
    \- colonies
    \- options
    \ - login / register
    \ - user options
    \- glowing grid
    \-  