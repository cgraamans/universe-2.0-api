uniform sampler2D skyboxTexture;  
varying vec2 vUV;

void main() {  
  vec4 sample = texture2D(skyboxTexture, vUV);
  gl_FragColor = vec4(sample.xyz, sample.w);
}