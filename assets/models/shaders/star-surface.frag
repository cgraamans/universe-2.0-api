#ifdef GL_ES
precision mediump float;
#endif

varying vec2 vUv;
varying vec3 vNormal;

uniform vec3 sunColorVec;
uniform sampler2D surfaceTexture;
uniform float time;

void main() {

	float paletteSpeed = 0.01;
    float pct = abs(sin(time)) * paletteSpeed;

	vec3 lookupColor = texture2D( surfaceTexture, vUv ).xyz;

	vec3 modifyColor = mix(sunColorVec,lookupColor,pct);	

	float intensity = 1.15 - dot( vNormal, vec3( 0.0, 0.0, 0.3 ) );
	vec3 outerGlow = vec3( 1.0, 0.8, 0.6 ) * pow( intensity, 10.0 );
	vec3 innerGlow = modifyColor * pow( intensity, pct );

	vec3 desiredColor = lookupColor + sunColorVec + outerGlow + innerGlow;

	gl_FragColor = vec4( desiredColor, 1.0 );

}
