module.exports = (app,$state,socketName,data)=>{

	let getCluster = (name) =>{ 

		return new Promise((res,rej)=>{

			if(!name) {

				let clusterIdSQL = 'SELECT dc.id, dc.name, dc.stub, gc.year, gc.skybox FROM `data.clusters` AS dc INNER JOIN `game.clusters` gc ON gc.cluster_id = dc.id WHERE isActive = 1 ORDER BY RAND() LIMIT 1';
				let passToCluster = [];

				app.DB.q(clusterIdSQL,passToCluster)
					.then(clusterIdArr=>{

						if(clusterIdArr.length>0){

							res(clusterIdArr[0]);

						} else {

							rej('no clusters found');

						}

					})
					.catch(e=>{

						rej(e);
					
					});


			} else {

				let clusterIdSQL = 'SELECT dc.id, dc.name, dc.stub, gc.year, gc.skybox FROM `data.clusters` AS dc INNER JOIN `game.clusters` gc ON gc.cluster_id = dc.id WHERE dc.stub = ? AND dc.isActive = 1';
				let passToCluster = [name];
				app.DB.q(clusterIdSQL,passToCluster)
					.then(clusterIdArr=>{

						if(clusterIdArr.length>0){

							res(clusterIdArr[0]);

						} else {

							getCluster()
								.then(clusterIdArr=>{
			
									if(clusterIdArr.length>0){

										res(clusterIdArr[0]);

									} else {

										rej('no clusters found');

									}

								})
								.catch(e=>{
			
									rej(e);
			
								});

						}

					})
					.catch(e=>{

						rej(e);
					
					});


			}

		});

	};

	return new Promise((resolve,reject)=>{

		let fnGeo = require('../models/global.geo.js')();

		let rtn = {ok:false};

		getCluster(data.name)
			.then(cluster=>{

				let clusterObj = Object.assign({},cluster);
				delete clusterObj.id

				rtn.cluster = clusterObj;

				rtn.ok = true;
				$state.socket.emit(socketName,rtn);

			})
			.catch(e=>{

				rtn.error = e;
				$state.socket.emit(socketName,rtn);

			});

	});

};