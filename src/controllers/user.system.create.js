module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		try {

			// work on model load
			let systemCreateModel = require('../models/system.create')(app);
			let systemPopulateModel = require('../models/system.populate')(app);

			let start = (new Date()).getTime();
			let step = 0;

			$state.socket.emit('user.system.create',{step:step,msg:'player system creation'});
			
			// console.log('AUTH REGISTER PASSTHROUGH',$state.user.auth);
			systemCreateModel.create(Math.floor(Math.random()*(9-5))+5)
				.then(systems=>{

					step++;
					$state.socket.emit('user.system.create',{step:step,elapsed:((new Date()).getTime()) - start,msg:'system created'});

					// console.log('systems',systems);
					
					// register random system as home of user
					// TODO: fix this so it's mostly yellow main sequences if poss..
					var userSystem = systems[Math.round(Math.random() * (systems.length -1))];
					
					/** TEST INSERT **/
					let gu = {
						user_id:$state.user.auth.id,
						home:userSystem.id
					};
					app.DB.q('INSERT INTO `game.users` SET ?',gu,true)
						.then(insertedUser=>{

							if(insertedUser.insertId){
							
								step++;
								$state.socket.emit('user.system.create',{step:step,elapsed:((new Date()).getTime()) - start,msg:'game user created'});

								systemPopulateModel.popSystem(userSystem.id)
									.then(planets=>{
										
										step++;
										$state.socket.emit('user.system.create',{step:step,elapsed:((new Date()).getTime()) - start,msg:'planets created'});

										app.DB.q('SELECT dc.name,dc.stub FROM `game.systems` AS gs INNER JOIN `data.clusters` dc ON dc.id = gs.cluster_id WHERE gs.id = ?',[userSystem.id])
											.then(clusterData=>{

												if(clusterData.length>0){
													
													step++;			
													$state.socket.emit('user.system.create',{
														'step':step,
														'elapsed':((new Date()).getTime()) - start,
														'cluster':clusterData[0],
														'system':{
															name:userSystem.name,
															stub:userSystem.stub
														}
													});

												} else {

													$state.socket.emit('user.system.create',{step:step,msg:'cluster not found',e:userSystem.stub});

												}
												resolve();


											})
											.catch(e=>{

												$state.socket.emit('user.system.create',{step:step,msg:'error retrieving system cluster',e:e});
											
											});


									})
									.catch(e=>{

										$state.socket.emit('user.system.create',{step:step,msg:'error populating system',e:e});
										resolve();

									});

							} else {

								$state.socket.emit('user.system.create',{step:step,msg:'problem inserting game user',e:true});
								resolve();

							}

						})
						.catch(e=>{
								
							$state.socket.emit('user.system.create',{step:step,msg:'error inserting game user',e:e});
							resolve();
						
						});

				})
				.catch(e=>{

					$state.socket.emit('user.system.create',{step:step,msg:'error creating system',e:e});
					resolve();

				});

		
		} catch(e){

			console.log('---------------- UNEXPECTED ERROR ----------------');
			console.log(e);

			reject(e);
		
		}
	
	});

};