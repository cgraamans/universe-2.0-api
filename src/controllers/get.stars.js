module.exports = (app,$state,socketName,data)=>{

	let getFocus = (stubCluster,stubStar,HasNoUserStar)=> {

		return new Promise((res,rej)=>{

			let sql = 'SELECT gs.id, gs.name, gs.stub, gs.posX, gs.posY, gs.posZ, gsd.`year`, gss.r0, gss.mass, gss.lum, gss.spec_trans, ds.name as star_name, ds.`type` as star_type, ds.K as star_temp, ds.spectral as star_spectral, ds.colorhex as star_color, ds.gl_min, ds.gl_max FROM `game.systems` AS gs INNER JOIN `game.system.data` gsd ON gsd.system_id = gs.id INNER JOIN `game.system.stars` gss ON gss.system_id = gs.id INNER JOIN `data.stars` ds ON ds.id = gss.star_id INNER JOIN `data.clusters` dc ON dc.id = gs.cluster_id ';
			let lookUp = [];

			if(!stubStar) {

				sql += 'WHERE gs.isActive = 1 AND dc.stub = ? ORDER BY RAND() LIMIT 1';
				lookUp.push(stubCluster);

				app.DB.q(sql,lookUp)
					.then(focalStar=>{

						res(focalStar[0]);

					})
					.catch(e=>{

						rej(e);

					});


			} else {

				if($state.user.auth && !HasNoUserStar) {

					getFocus(stubCluster,null,true)
						.then(focalStarRecurse=>{

							res(focalStarRecurse);
						
						})
						.catch(e=>{
						
							rej(e);
						
						});			


				} else {

					sql += 'WHERE gs.stub = ? AND gs.isActive = 1 LIMIT 1';
					lookUp.push(stubStar);

					app.DB.q(sql,lookUp)
						.then(focalStar=>{

							if(focalStar.length < 1){

								getFocus(clusterStub)
									.then(focalStarRecurse=>{

										res(focalStarRecurse);
									
									})
									.catch(e=>{
									
										rej(e);
									
									});

							} else {

								res(focalStar[0]);

							}
						
						})
						.catch(e=>{

							rej(e);

						});

				}


			}

		});

	};

	return new Promise((resolve,reject)=>{

		let fnGeo = require('../models/global.geo.js')();
		let rtn = {ok:false};

		if(!data.cluster) {

			rtn.error = 'no cluster';
			$state.socket.emit(socketName,rtn);

		} else {

			getFocus(data.cluster,data.star)
				.then(focus=>{

					let focusObj = Object.assign({},focus);
					delete focusObj.id

					rtn.stars = [focusObj];

					let bb = fnGeo.boundingBox(5,focus.posX,focus.posY,focus.posZ);
					let bbQL = 'SELECT gs.name, gs.stub, gs.posX, gs.posY, gs.posZ, gsd.`year`, gss.r0, gss.mass, gss.lum, gss.spec_trans, ds.name as star_name, ds.`type` as star_type, ds.K as star_temp, ds.spectral as star_spectral, ds.colorhex as star_color, ds.gl_min, ds.gl_max FROM `game.systems` AS gs INNER JOIN `data.clusters` dc ON dc.id = gs.cluster_id INNER JOIN `game.system.data` gsd ON gsd.system_id = gs.id INNER JOIN `game.system.stars` gss ON gss.system_id = gs.id INNER JOIN `data.stars` ds ON ds.id = gss.star_id WHERE gs.id != ? AND dc.stub = ? AND gs.posX > ? AND gs.posX < ? AND gs.posY > ? AND gs.posY < ? AND gs.posZ > ? AND gs.posZ < ?';
					let bbLookup = [focus.id, data.cluster, bb.x.min, bb.x.max, bb.y.min, bb.y.max, bb.z.min, bb.z.max];

					app.DB.q(bbQL,bbLookup)
						.then(starList=>{

							starList.forEach(star=>{
								rtn.stars.push(star);
							});

							rtn.ok = true;

							$state.socket.emit(socketName,rtn);

						})
						.catch(e=>{

							console.log(e);

							rtn.error = e;
							$state.socket.emit(socketName,rtn);

						});

				})
				.catch(e=>{

					rtn.error = e;
					$state.socket.emit(socketName,rtn);

				});

		}

	});

};