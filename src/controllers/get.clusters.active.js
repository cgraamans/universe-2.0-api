module.exports = (app,$state,socketName,data)=>{

	return new Promise((resolve,reject)=>{

		let getSQL = 'SELECT name,stub,`year`,sysCount as systems FROM `game.clusters` AS gc INNER JOIN `data.clusters` dc ON dc.id = gc.cluster_id LEFT JOIN (SELECT COUNT(*) as sysCount, cluster_id FROM `game.systems` GROUP BY cluster_id) AS sc ON sc.cluster_id = gc.cluster_id WHERE dc.isActive = 1 ORDER BY sysCount DESC'
		if($state.user.auth){}

		app.DB.q(getSQL,[])
			.then(clusterList=>{
				if(clusterList.length>0) $state.socket.emit(socketName,{ok:true,clusters:clusterList});
				resolve(data);
			})
			.catch(e=>{
				reject(e);
			});

	});

};	