module.exports = (app,$state,socketName,data)=>{

	let getObj = (focus)=>{

		return new Promise((resolve,reject)=>{
			if(!focus){

				reject('no focus');
			
			} else {

				let rtn = {ok:false,o:[],d:[],f:focus};
				
				let p = [
					
					// o - static objects
					new Promise((res,rej)=>{
					
						app.DB.q('SELECT gso.name, gso.stub, gso.`type`, gso.size, gso.mass, gso.percentWater,gso.r,gso.a,gso.e,gso.i,gso.w,gso.Ω,gso.M,gso.t,gso.isSolid,gsoP.stub AS parent FROM `game.system.obj` AS gso LEFT JOIN `game.system.obj` gsoP ON gsoP.id = gso.parent_id INNER JOIN `game.systems` gs ON gs.id = gso.system_id WHERE gs.stub = ?',[focus])
							.then(oRes=>{
					
								if(oRes.length>0){
									rtn.o = oRes;
								}
								res();
					
							})
							.catch(e=>{
					
								rej(e);
					
							});
					}),
					
					// d - dynamic objects
					new Promise((res,rej)=>{

						res();
					
					}),

				];

				Promise.all(p)
					.then(()=>{

						rtn.ok = true;
						resolve(rtn);
					
					})
					.catch(e=>{

						reject(e);

					});

			}

		});

	};

	return new Promise((resolve,reject)=>{

		let fnGeo = require('../models/global.geo.js')();
		let rtn = {ok:false};

		getObj(data.focus)
			.then(focusObjArr=>{

				console.log('focusObjArr',focusObjArr)

				$state.socket.emit(socketName,focusObjArr);
				resolve();

			})
			.catch(e=>{

				rtn.error = e;
				$state.socket.emit(socketName,rtn);
				reject(e);

			});

	});

};