'use strict';

let systemPopulate = class systemPopulate {

	constructor(app) {

		this.options = {

			systemNameLimit:10, // maximum itterations for name finding
			systemNameOptionalPrefix:25, // percentage for optional fixed prefix
			systemNameprefixCount:1000, // prefix max
			
			posBoundingBox:1.00, // star collision bounding box
			posRecurseLimit:10, // maximum itterations
			posDecimalLimit:4,	// maximum decimal places

		};

		this.app = app;
		this.dataStore = {};
		this.error = false;

		this.geo = require('./global.geo.js')();
		this.maths = require('./global.maths.js')();

		try {

			let fs = require('fs');
			this.dataStore.obj = JSON.parse(fs.readFileSync('./assets/json/obj.json', 'utf8'));

		} catch(e) {

			this.error = e;

		}

	}

	// https://stackoverflow.com/questions/9083037/convert-a-number-into-a-roman-numeral-in-javascript
	convertToRoman(num) {

	  var roman =  {"M" :1000, "CM":900, "D":500, "CD":400, "C":100, "XC":90, "L":50, "XL":40, "X":10, "IX":9, "V":5, "IV":4, "I":1};
	  var str = "";

	  for (var i of Object.keys(roman) ) {
	    var q = Math.floor(num / roman[i]);
	    num -= q * roman[i];
	    str += i.repeat(q);
	  }

	  return str;

	}

	objSetTypeProp(prop,obj){
				
		if(this.dataStore.obj.props[prop]){
		
			if(obj.props && obj.props[prop]){
		
				return this.maths.randFromMinMax(obj.props[prop].min,obj.props[prop].max,obj.props[prop].div);
		
			} else {

		
				return this.maths.randFromMinMax(this.dataStore.obj.props[prop].min,this.dataStore.obj.props[prop].max,this.dataStore.obj.props[prop].div);
		
			}			
		
		} else {

			if(obj.props) return this.objSetType(prop,obj.props);			
			return null;

		}
	
	}

	objSetType(prop,obj){
		// console.log('objSetType',prop,obj);
		if(obj[prop]) return this.maths.randFromMinMax(obj[prop].min,obj[prop].max,obj[prop].div);
		return null;

	}

	// recursive walk through creating subobjects
	subobjWalk(obj,subTypeArr){

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				if(!subTypeArr || subTypeArr.length < 1){

					subTypeArr = that.dataStore.obj.subtypes[obj.t];

				}
				if(subTypeArr && subTypeArr.length > 0){

					let subTPromArr = [];
					subTypeArr.forEach(subType=>{

						let chance = 100;
						if(subType.chance){
							chance = subType.chance;
						}
						let runNumberOfObj = 0;
						if(that.maths.d100() < chance){
							runNumberOfObj = Math.floor(that.maths.randFromMinMax(subType.min,subType.max));
						}

						let planetaryObjJSON = that.dataStore.obj.types[subType['type']];
						let planetaryObjOrder = {
							mass:[],
							a:[]
						};
						if(planetaryObjJSON){

							subTPromArr.push(new Promise((res,rej)=>{

								// console.log('SUBOBJ OBJ GENERATE',runNumberOfObj,subType['type'],planetaryObjJSON,planetaryObjOrder,obj['id']);

								that.objGenerate(runNumberOfObj,subType['type'],planetaryObjJSON,planetaryObjOrder,obj['id'])
									.then(generatedObjects=>{

										let pushToArr = {objs:generatedObjects,subtypes:[]};
										if(subType.subtypes && subType.subtypes.length > 0) {

											pushToArr.subtypes = subType.subtypes;

										}
										res(pushToArr);

									})
									.catch(e=>{

										rej(e);

									});

							}));

						}

					});

					Promise.all(subTPromArr)
						.then(subtypeResultArr=>{

							let nodesObjArr = [].concat.apply([], subtypeResultArr);
							let nodesPArr = [];
							nodesObjArr.forEach(gO=>{
							
								nodesPArr.push(new Promise((reso,reje)=>{

									let nodeInsP = [];
									gO.objs.forEach((node,nodeC)=>{

										nodeInsP.push(new Promise((res,rej)=>{

											node.name = obj.name + "-" + nodeC;
											node.stub = obj.stub + "-" + nodeC;
											node.name_original = node.name;
											node.system_id = obj.system_id;

											that.app.DB.q("INSERT INTO `game.system.obj` SET ?",node,true)
												.then(insertedNode=>{

													if(gO.subtypes && gO.subtypes.length>0){

														let walkObj = {
															id:insertedNode.insertId,
															system_id:obj.system_id,
															name:node.name,
															stub:node.stub,
															t:node.type
														};

														// console.log('SUBTYPE WALKBOJ',walkObj,gO.subtypes);

														that.subobjWalk(walkObj,gO.subtypes)
															.then(()=>{
																
																res();

															})
															.catch(e=>{

																rej(e);
															
															}); // that.subobjWalk(walkObj,sT)

													} else {

														res();

													}

												})
												.catch(e=>{

													rej(e);

												}); // that.app.DB.q("INSERT INTO `game.system.obj` SET ?",node,true)

										}));

									});
									Promise.all(nodeInsP)
										.then(()=>{

											reso();

										})
										.catch(e=>{

											reje(e);

										}); // Promise.all(nodeInsP)

								}));

							});
							Promise.all(nodesPArr)
								.then(()=>{

									resolve();

								})
								.catch(e=>{

									reject(e);

								}); // Promise.all(nodesPArr)

						})
						.catch(e=>{

							reject(e);

						}); // Promise.all(subTPromArr)

				} else {

					reject('SubType not found for '+obj.t);

				}

			} catch(e){

				reject(e);

			}

		});

	}

	// number of bjects, type of object, the json of that object from obj.json and the order {mass:[],a:[]}
	objGenerate(runNumberOfObj,typeOfPlanet,planetaryObjJSON,planetaryOrder,parentId){

		let that = this;
		return new Promise((resolve,reject)=>{

			let objArray = [];
			let pArray = [];

			//
			// main generator
			//
			for(let i=0;i<runNumberOfObj;i++){

				pArray.push(new Promise((res,rej)=>{

					try {

						let obj = {
							type:typeOfPlanet,
							parent_id:null
						};
						if(parentId) obj.parent_id = parentId; 
						
						// mass & semi-major axis
						if(planetaryOrder.mass.length>0 && planetaryOrder.a.length>0){

							obj.a = planetaryOrder.a[i];
							obj.mass = planetaryOrder.mass[i];

						} else {

							obj.a = that.objSetTypeProp('a',planetaryObjJSON);
							obj.mass = that.objSetType('mass',planetaryObjJSON);
							// console.log("SETTYPE MASS",planetaryObjJSON);
						}

						// surface solidity boolean
						planetaryObjJSON.solid ? obj.isSolid = planetaryObjJSON.solid : obj.isSolid = 0;
						
						// percentWater of object
						let hasWater = null;
						obj.percentWater = 0;

						if(planetaryObjJSON.pctOfWater) hasWater = that.maths.d100(planetaryObjJSON.pctOfWater);
						if(hasWater !== null) obj.percentWater = that.maths.randFromMinMax(0,700,10);

						// Semi major axis
						obj.Ω = that.objSetTypeProp('Ω',planetaryObjJSON);
						
						// Mstart
						obj.M = that.objSetTypeProp('M',planetaryObjJSON);
						
						// inclination
						obj.i = that.objSetTypeProp('i',planetaryObjJSON);
						
						// eccentricity
						obj.e = that.objSetTypeProp('e',planetaryObjJSON);
						
						// arg of perihelion
						obj.w = that.objSetTypeProp('w',planetaryObjJSON);
						
						// tilt
						obj.t = that.objSetTypeProp('t',planetaryObjJSON);

						obj.r = null;
						
						if(planetaryObjJSON['size'] && planetaryObjJSON['size'].length>0){

								let sizeArr = planetaryObjJSON['size'];

								let roll = that.maths.d100();
								let fortune = 0;
								let resSize = sizeArr[0];

								for(let j=0,z=sizeArr.length;j<z;j++){

									fortune += sizeArr[j].pct;
									if(roll - fortune >= 0){
										resSize = sizeArr[j];										
									}

								}

								if(resSize && resSize.conditional){
									obj.r = that.maths.randFromMinMax(resSize.conditional.min,resSize.conditional.max,resSize.conditional.div);	
									obj['size'] = resSize['type'];
								}

						}

						// console.log('OBJ',obj);
						res(obj);

					} catch(e) {

						rej(e);

					}

				}));

			}

			Promise.all(pArray)
				.then(arrayOfObj=>{
					resolve(arrayOfObj);
				})
				.catch(e=>{
					reject(e);
				});

		});

	}

	popSystem(systemId){

		let that = this;
		return new Promise((resolve,reject)=>{

			if(that.error) {

				reject(that.error);

			} else {

				// first get the number of max planetary objects
				that.app.DB.q('SELECT gas,rock,fieldAsteroids,fieldDebris,comet FROM `game.system.stars` AS gss INNER JOIN `data.stars` ds ON gss.star_id = ds.id INNER JOIN `data.star.obj` dso ON dso.star_id = ds.id WHERE gss.system_id = ?',[systemId])
					.then(planetaryTypesForThisStar=>{

						if(planetaryTypesForThisStar.length===1){

							let planetaryObjects = [];
							let pList = planetaryTypesForThisStar[0];
							let planetaryObjPromiseArr = [];

							for(let planetaryObjType in pList){

								if(pList[planetaryObjType]>0 && that.dataStore.obj.types[planetaryObjType]){

									let planetaryObjJSON = that.dataStore.obj.types[planetaryObjType];
									let planetaryObjCount = Math.round(Math.random()*(pList[planetaryObjType]-1))+1;
								
									let planetaryObjOrder = {
										mass:[],
										a:[]
									};

									// semi-major axis (a) and mass are linked if rock or gas
									if(['gas','rock'].indexOf(planetaryObjType) > -1 && planetaryObjJSON.props && planetaryObjJSON.props.a && planetaryObjJSON.mass) {
									
										for(let i=0;i<planetaryObjCount;i++){

											planetaryObjOrder.a.push(
												that.maths.randFromMinMax(
													planetaryObjJSON.props.a.min,
													planetaryObjJSON.props.a.max,
													planetaryObjJSON.props.a.div
												)
											);

											planetaryObjOrder.mass.push(
												that.maths.randFromMinMax(
													planetaryObjJSON.mass.min,
													planetaryObjJSON.mass.max,
													planetaryObjJSON.mass.div
												)
											);

										}

										// sort the objects (mass: high to low, a: low to high)
										planetaryObjOrder.mass.sort((a,b)=>{
											return a - b;
										});
										planetaryObjOrder.a.sort((a,b)=>{
											return b - a;
										});							

									}

									// generate top nodes for this type
									planetaryObjPromiseArr.push(that.objGenerate(planetaryObjCount,planetaryObjType,planetaryObjJSON,planetaryObjOrder));

								}

							}
							Promise.all(planetaryObjPromiseArr)
								.then(planetaryObjectsGenerated=>{
									
									// concat all and sort by semi-major axis (a)
									let topNodes = [].concat.apply([], planetaryObjectsGenerated);
									topNodes.sort((a,b)=>{
										return a.a - b.a;
									});
									
									// name top nodes
									that.app.DB.q('SELECT name,stub FROM `game.systems` WHERE id = ?',[systemId])
										.then(starName=>{

											if(starName.length === 1){

												let nameList = starName[0];
												let insPList = []; 
												for(let i=0,c=topNodes.length;i<c;i++){
													
													let roman = that.convertToRoman(i+1);

													topNodes[i].name = nameList.name + ' ' + roman;
													topNodes[i].name_original = topNodes[i].name;
													topNodes[i].stub = nameList.stub + '-' + roman;
													topNodes[i].system_id = systemId;

													insPList.push(new Promise((res,rej)=>{

														// insert top nodes
														that.app.DB.q("INSERT INTO `game.system.obj` SET ?",topNodes[i],true)
															.then(resOfTNI=>{

																if(resOfTNI.insertId > 0){
																	res();
																} else {
																	rej(topNodes[i].name);
																}

															})
															.catch(e=>{
																rej(e);
															})


													}));


												}

												Promise.all(insPList)
													.then(()=>{
														
														// get top nodes w/ ids.
														that.app.DB.q('SELECT id,name,stub,`type` as t,system_id FROM `game.system.obj` WHERE system_id = ?',[systemId])
															.then(idTopNodes=>{

																let subobjPromises = [];
																idTopNodes.forEach(topNodeWID=>{

																	// console.log(topNodeWID);
																	if(that.dataStore.obj.subtypes[topNodeWID.t]){

																		subobjPromises.push(new Promise((res,rej)=>{

																			that.subobjWalk(topNodeWID)
																				.then(subnodes=>{

																					res();

																				})
																				.catch(e=>{
																					rej(e);
																				})

																		}));

																	}

																});
																if(subobjPromises.length>0){

																	Promise.all(subobjPromises)
																		.then(subObjs=>{

																			resolve();

																		})
																		.catch(e=>{

																			reject(e);
																		
																		}); // Promise.all(subobjPromises)

																} else {

																	resolve();

																}
																
															})
															.catch(e=>{

																reject(e);

															}); // that.app.DB.q('SELECT id,name,stub,`type` as t FROM `game.system.obj` WHERE system_id = ?',[systemId]) 

													})
													.catch(e=>{

														reject(e);

													}); // Promise.all(insPList)


											} else {

												reject('no star');

											}


										})
										.catch(e=>{

											reject(e);

										}); // that.app.DB.q('SELECT name,stub FROM `game.systems` ...
									

								})
								.catch(e=>{

									reject(e);
					
								}); // Promise.all(planetaryObjPromiseArr)


						} else {

							reject('no star');

						}

					})
					.catch(e=>{

						reject(e);

					}); // that.app.DB.q('SELECT gas,rock,fieldAsteroids,fieldDebris,comet FROM `game.system.stars` ...

			}

		});

	}


}

module.exports = app=> {return new systemPopulate(app)};