'use strict';

let geoClass = class geo {

	constructor(){}

	toCartesian(r,theta,phi,x,y,z){

		if(!x) x=0;
		if(!y) y=0;
		if(!z) z=0;
		if(!theta) theta = Math.PI * 2 * Math.random();
		if(!phi) phi = Math.PI * Math.random();

		return {
			x:(r *  Math.cos(theta) * Math.sin(phi))+x,
			y:(r * Math.sin(theta) * Math.sin(phi))+y,
			z:(r * Math.cos(phi))+z
		};

	}



	//  ( x-cx ) ^2 + (y-cy) ^2 + (z-cz) ^ 2 < r^2 

	boundingBox(size,x,y,z){

		if(!x) x=0;
		if(!y) y=0;
		if(!z) z=0;

		return {
			x:{
				min:x-size,
				max:x+size,
			},
			y:{
				min:y-size,
				max:y+size,
			},
			z:{
				min:z-size,
				max:z+size,
			},

		};

	}

};

module.exports = ()=>{
	return new geoClass();
};