'use strict'; 

let mathClassObj = class mathClass {

	constructor(){}

	d100(val){

		if(!val) return Math.floor(Math.random() * 100) +1;

		if(val<0) val = 0;
		if(val>100) val = 100;
		
		return Math.floor(Math.random() * 100) +1 <= val ? true : false;
		
	}

	d1000(){

		return Math.floor(Math.random() * 1000) +1;
	
	}

	randFromMinMax(min,max,div){
	
		if(!div) div = 1;

		let res = Math.random() * (max - min) + min;
		return res > 0 ? res/div : 0;
	
	}

	randJitter(val,div,jitter){

		if(!div) div=1;
		if(!jitter) jitter = 20;
		
		let pct = val/100;
		return this.randFromMinMax(val - (pct*jitter),val +(pct*jitter),div); 

	}

	randArrIndex(itemLength){

		return Math.round(Math.random() * (itemLength-1));

	}

	unixTime(){

		return Math.round((new Date()).getTime()/1000);
	
	}

	roundTo(num,decimal){
	
		return +(Math.round(num + "e+"+decimal)  + "e-"+decimal);
	
	}

	randStub(num) {
		
		if(!num) num = 5
		return Math.random().toString(36).replace(/[^a-z]+/g, '').substr(0, num);

	}

};

module.exports = ()=>{
	
	return new mathClassObj();

};