'use strict';

let systemCreate = class systemCreate {

	constructor(app) {

		this.options = {

			systemNameLimit:10, // maximum itterations for name finding
			systemNameOptionalPrefix:25, // percentage for optional fixed prefix
			systemNameprefixCount:1000, // prefix max

			stepsMaxDistance:5,

			posDistance:{ // distance factors for stars
				min:101,
				max:500,
				div:100
			},
			
			posBoundingBox:1.00, // star collision bounding box
			posRecurseLimit:10, // maximum itterations
			posDecimalLimit:4,	// maximum decimal places

		};

		this.app = app;
		this.data = {};
		this.error = false;

		this.geo = require('./global.geo.js')();
		this.maths = require('./global.maths.js')();

		try {

			let fs = require('fs');
			this.data.names = JSON.parse(fs.readFileSync('./assets/json/names.json', 'utf8'));

		} catch(e) {

			this.error = e;

		}

	}

	create(n,clusterId){

		if(!n) n = 0;
		let that = this;

		if(!clusterId) {

			return new Promise((resolve,reject)=>{

				that.clusterRandom()
			
					.then(newClusterId=>{
			
						that.create(n,newClusterId)
			
							.then(systems=>{
			
								resolve(systems);
			
							})
							.catch(e=>{
			
								reject(e);
			
							});

					})
					.catch(e=>{
			
						reject(e);
			
					});

			});

		} else {

			return new Promise((resolve,reject)=>{

				let p = [

						// check/insert game.clusters.
						new Promise((res,rej)=>{

							that.app.DB.q('SELECT id FROM `game.clusters` WHERE cluster_id = ?',[clusterId])
								.then(gameCluster=>{
									if(gameCluster.length<1){
										that.app.DB.q('INSERT INTO `game.clusters` SET ?',{
											cluster_id:clusterId,
											skybox:(Math.floor(Math.random()*9)+1)
										},true)
										.then(gcInsert=>{
											if(gcInsert.insertId){
												res();
											} else {
												rej(gcInsert);
											}
										})
										.catch(e=>{
											rej(e);
										});
									} else {
										res();
									}
								})
								.catch(e=>{
									rej(e);
								});

						})

					],
					namedSystems = [];
				for(let i=0;i<n;i++){

					p.push(new Promise((res,rej)=>{

						that.systemName(clusterId)
							.then(name=>{

								namedSystems.push({
									systemName:name,
									cluster:clusterId,
								});
								res();

							})
							.catch(e=>{
								rej(e)
							});

					}));

				}
				Promise.all(p)
					.then(()=>{
						
						that.position(namedSystems)
							.then(positionedObjects=>{

								if(positionedObjects){

									let pI = [],
										insertedObjects = [];
									for(let i = 0;i<positionedObjects.systems.length;i++){

										let positionedObject = positionedObjects.systems[i];

										pI.push(new Promise((res,rej)=>{

											let objStub = that.maths.randStub().trim()+'-'+(positionedObject.systemName.toLowerCase().trim().split(' ').join(''));

											let objToIns = {
												cluster_id:positionedObject.cluster,
												stub:objStub,
												name:positionedObject.systemName,
												name_original:positionedObject.systemName,
												dt_created:that.maths.unixTime(),
												posX:that.maths.roundTo(positionedObject.coords.x,that.options.posDecimalLimit),
												posY:that.maths.roundTo(positionedObject.coords.y,that.options.posDecimalLimit),
												posZ:that.maths.roundTo(positionedObject.coords.z,that.options.posDecimalLimit),
											};
											that.insertSystem(objToIns)
												.then(insertId=>{

													res({
														id:insertId,
														cluster_id:objToIns.cluster_id,
														name:objToIns.name,
														stub:objToIns.stub,
														coords:{
															x:objToIns.posX,
															y:objToIns.posY,
															z:objToIns.posZ,
														}
													});

												})
												.catch(e=>{
													
													rej(e);
												
												});

										}));

									}
									Promise.all(pI)
										.then(insertedObjects=>{

											that.setStars(insertedObjects)
												.then(returnedStars=>{

													that.setMapLead(positionedObjects.mapId)
														.then(()=>{

															resolve(returnedStars);

														})
														.catch(e=>{
															reject(e);		
														});

												})
												.catch(e=>{

													that.setMapLead(positionedObjects.mapId)
														.then(()=>{
															reject(e);
														})
														.catch(e=>{
															reject(e);
														});

												});

										})
										.catch(e=>{

											that.setMapLead(positionedObjects.mapId)
												.then(()=>{
													reject(e);
												})
												.catch(e=>{
													reject(e);		
												});
										
										});

								} else {

									reject('no positions possible for this cluster')

								}

							})
							.catch(e=>{

								reject(e);
							
							});
					
					})
					.catch(e=>{
					
						reject(e);
					
					});

			});

		}

	}

	clusterRandom() {

		let that = this;
		return new Promise((resolve,reject)=>{

			that.app.DB.q('SELECT id FROM `data.clusters` WHERE isActive = 1 ORDER BY RAND() LIMIT 1')
				.then(clusterIdArr=>{
					if(clusterIdArr.length===1) {
						resolve(clusterIdArr[0].id);
					} else {
						reject('no random cluster');
					}
				})
				.catch(e=>{
					reject(e);
				});

		});

	}

	systemName(clusterId,n) {

		let that = this;
		if(!n) n = that.options.systemNameLimit;
		return new Promise((resolve,reject)=>{

			if(n>0) {

				let systemName = this.data.names.objects.cities[that.maths.randArrIndex(this.data.names.objects.cities.length)];
				
				if(that.maths.d100(that.options.systemNameOptionalPrefix)){
					systemName = this.data.names.modifiers.prefix[that.maths.randArrIndex(this.data.names.modifiers.prefix.length)]+' '+systemName;	
				}

				let prefix = that.maths.randStub(2).toUpperCase() + that.maths.d1000() + that.maths.randStub(1).toLowerCase();
				if(prefix>this.options.systemNameprefixCount) prefix = this.options.systemNameprefixCount;

				systemName = prefix + ' - ' + systemName;

				that.app.DB.q('SELECT id from `game.systems` WHERE name = ? AND `name_original` = ?',[systemName,systemName],true)
					.then(nameCheck=>{
						if(nameCheck.length>0){

							n--;
							that.systemName(clusterId,n)
								.then(name=>{

									resolve(name);
								
								})
								.catch(e=>{
						
									reject(e);
						
								});

						} else {

							resolve(systemName);

						}

					})
					.catch(e=>{

						reject(e);
					
					});

			} else {

				resolve();

			}

		});

	}

	positionSystemRecurse(system,radius,startX,startY,startZ,n){

		let that = this;
		return new Promise((resolve,reject)=>{

			try {


				if(!n) n=0;
				if(n<that.options.posRecurseLimit) {
					
					system.coords = that.geo.toCartesian(radius,null,null,startX,startY,startZ);
					let boundingBox = that.geo.boundingBox(that.options.posBoundingBox,system.coords.x,system.coords.y,system.coords.z);

					that.app.DB.q('SELECT id FROM `game.systems` WHERE posX > ? AND posX < ? AND posY > ? AND posY < ? AND posZ > ? AND posZ < ?',[boundingBox.x.min,boundingBox.x.max,boundingBox.y.min,boundingBox.y.max,boundingBox.z.min,boundingBox.z.max])
						.then(systemsNearby=>{

							if(systemsNearby.length>0){

								n++;
								that.positionSystemRecurse(system,radius,startX,startY,startZ,n)
									.then(recursedSystemsNearby=>{

										resolve(recursedSystemsNearby);
									})
									.catch(e=>{
										reject(e);
									});

							} else {
								resolve(system);
							}
						})
						.catch(e=>{
							reject(e);
						});
				} else {
					resolve();
				}

			} catch(e) {

				reject(e);

			}

		});

	}

	positionWalk(systemsIn,mapLeads,systemsOut,n){

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				if(!systemsOut) systemsOut = [];
				if(!n) n = 0;
				
				if(systemsIn.length === 0) {

					resolve({
						mapLeads:mapLeads,
						systems:systemsOut
					});

				} else {

					if(n < that.options.posRecurseLimit) {

						// random system, random
						let idxSystemToMove = that.maths.randArrIndex(systemsIn.length);
						let idxmapLeadToUse = that.maths.randArrIndex(mapLeads.length);
						let r = that.maths.randFromMinMax(that.options.posDistance.min,that.options.posDistance.max,that.options.posDistance.div);

						that.positionSystemRecurse(systemsIn[idxSystemToMove],r,mapLeads[idxmapLeadToUse].x,mapLeads[idxmapLeadToUse].y,mapLeads[idxmapLeadToUse].z)
							.then(positionedSystem=>{

								if(positionedSystem){

									systemsOut.push(positionedSystem);
									mapLeads.push({
										x:positionedSystem.coords.x,
										y:positionedSystem.coords.y,
										z:positionedSystem.coords.z
									});
									systemsIn.splice(idxSystemToMove,1);

									that.positionWalk(systemsIn,mapLeads,systemsOut)
										.then(posWalkResult=>{

											resolve(posWalkResult);

										})
										.catch(e=>{

											reject(e);

										});

								} else {

									n++;
									that.positionWalk(systemsIn,mapLeads,systemsOut,n)
										.then(posWalkResult=>{

											resolve(posWalkResult);

										})
										.catch(e=>{

											reject(e);

										});

								}

							})
							.catch(e=>{

								reject(e);

							})
						
					} else {

						resolve();

					}
					
				}

			} catch(e) {

				reject(e);

			}

		});

	}

	position(systems,n) {

		if(!n) n = 0;
		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				that.mapLead(systems)
					.then(mapLeadObj=>{

						that.positionWalk(mapLeadObj.systems,[mapLeadObj.mapLead])
							.then(positionedSystems=>{

								if(positionedSystems) positionedSystems.mapId = mapLeadObj.mapId;
								if(!positionedSystems) {

									if(n<that.options.posRecurseLimit){

										n++;
										that.position(systems,n)
											.then(posRecurse=>{

												resolve(posRecurse);

											})
											.catch(e=>{

												reject(e);
										
											});

									} else {

										resolve(systems);

									}

								} else {

									resolve(positionedSystems);	
								
								}

							})
							.catch(e=>{

								reject(e);
							
							});
						
					})
					.catch(e=>{
						reject(e);
					});

			} catch(e) {
				reject(e);
			}

		});

	}

	mapLead(systems){

		let that = this;
		return new Promise((resolve,reject)=>{

			try {

				that.app.DB.q('SELECT gs.id, gs.posX as x, gs.posY as y, gs.posZ as z FROM `game.systems` AS gs INNER JOIN `game.system.data` gsd ON gsd.system_id = gs.id WHERE gs.cluster_id = ? AND isMapLead = 0 ORDER BY RAND() LIMIT 1',[systems[0].cluster])
					.then(mapLead=>{

						if(mapLead.length===1) {

							let mapLeader = mapLead[0];
							that.setMapLead(mapLeader.id,1)
								.then(()=>{

									resolve({
										mapId:mapLeader.id,
										mapLead:mapLeader,
										systems:systems
									});
								
								})
								.catch(e=>{
									
									reject(e);
								
								});

						} else {

							if(systems.length > 1){

								let randSystem = that.maths.randArrIndex(systems.length);
								let cleanMapLead = systems[randSystem];
								
								systems.splice(randSystem,1);
								
								let objStub = (that.maths.randStub()+'-'+(cleanMapLead.systemName.toLowerCase())).replace(' ','-');
								let insSystemsObj = {
									stub:objStub,
									name:cleanMapLead.systemName,
									name_original:cleanMapLead.systemName,
									cluster_id:cleanMapLead.cluster,
									dt_created:that.maths.unixTime()
								};

								that.insertSystem(insSystemsObj)
									.then(sysId=>{

										resolve({

											mapLead:{
												x:0,
												y:0,
												z:0,
											},
											systems:systems,
											mapId:sysId,

										});

									})
									.catch(e=>{

										reject(e);

									});

							} else {

								reject('no map lead');

							}

						}

					})
					.catch(e=>{

						reject(e);
					
					});

			} catch(e) {

				reject(e);

			}

		});

	}

	insertSystem(system){

		let that = this;
		return new Promise((resolve,reject)=>{

			that.app.DB.q('INSERT INTO `game.systems` SET ?',system,true)
				.then(insSys=>{

					if(insSys.insertId){

						let insertedSystemId = insSys.insertId;
						that.app.DB.q('INSERT INTO `game.system.data` SET ?',{system_id:insertedSystemId},true)
							.then(insData=>{
								if(insData.insertId) {

									resolve(insData.insertId);

								} else {

									reject('error inserting system');

								}

							})
							.catch(e=>{

								reject(e)
							
							});

					} else {

						reject('error inserting mapLead into game systems');

					}

				})
				.catch(e=>{
					
					reject(e)
				
				});

		});

	}

	setStars(systems){


		let that = this;
		return new Promise((resolve,reject)=>{

			let rtn = [];
			that.app.DB.q('SELECT id, avg_r0 as r0, avg_mass as mass, avg_lum as lum, avg_spec_trans as spec_trans from `data.stars`')
				.then(starData=>{

					if(starData.length > 0){

						let sP = [];
						for(let i = 0;i<systems.length;i++){

							let system = systems[i],
								star = starData[that.maths.randArrIndex(starData.length)];

							sP.push(new Promise((res,rej)=>{

								let insertObj = {
									system_id:system.id,
									star_id:star.id,
									r0:that.maths.randJitter(star.r0),
									mass:that.maths.randJitter(star.mass),
									lum:that.maths.randJitter(star.lum),
									spec_trans:that.maths.randJitter(star.spec_trans),
								};

								that.app.DB.q('INSERT INTO `game.system.stars` SET ?',insertObj)
									.then(ins=>{
									
										if(ins.insertId){
										
											system.star_id = ins.insertId;
											rtn.push(system);

											res();
									
										} else {
									
											rej('insert failed');
									
										}
									
									})
									.catch(e=>{

										rej(e);
									
									});

							}));

						}
						Promise.all(sP)
							.then(()=>{

								resolve(rtn);

							})
							.catch(e=>{

								reject(e);

							});

					} else {

						reject('no star data');
					
					}

				})
				.catch(e=>{

					reject(e);
				
				});

		});

	}

	setMapLead(mapLeaderId,isMapLead){

		let that = this;
		return new Promise((resolve,reject)=>{

			try {
				if(isMapLead && isMapLead !== 0) isMapLead = 1;
				if(!isMapLead) isMapLead = 0;

				that.app.DB.q('UPDATE `game.system.data` SET isMapLead = ? WHERE system_id = ?',[isMapLead,mapLeaderId],true)
					.then(updateMapLead=>{
						if(updateMapLead.affectedRows === 1) {

							resolve();

						} else {

							reject('setMapLead not updated');

						}

					})
					.catch(e=>{

						reject(e);
					
					});

				} catch(e) {

					reject(e);

				}

		});

	}

}

module.exports = app=> {return new systemCreate(app)};